import com.hp.lft.sdk.GeneralLeanFtException;
import com.hp.lft.sdk.java.Label;
import com.hp.lft.sdk.mobile.*;
import com.hp.lft.sdk.mobile.*;
import com.hp.lft.sdk.mobile.Button;
import com.hp.lft.sdk.mobile.Keys;
import com.hp.lft.sdk.sap.gui.ToolBar;
import org.junit.*;
import com.hp.lft.sdk.*;
import com.hp.lft.verifications.*;
import unittesting.*;
import java.awt.*;





public class MainClass {

    /////////////////////////////////С П И С О К  Д Е В А Й С О В/////////////////////////////////
    //Идентификатор приложение eMotion:
    public final static String eMotionIdentifier = "ru.megalabs.multifon";
    //Девайс LGH870:
    public final static String iDLGH870DS67e49aee = "LGH870DS67e49aee";
    //Девайс samsung SM-N950F:
    public final static String iDSamsungSMN950F = "ce06171648499531047e";
    //Девайс samsung SM-J730FM:
    public final static String iDSamsungSMN730F = "5200e2f2ec0ab449";

    //Девайс samsung SM-G955F <------------- И С П О Л Ь З У Е М Ы Й --------
    public final static String iDSamsungSMN955F = "ce0317134a784c550c";
    //msisdn samsung SM-G955F, который используется для вставки при регистрации:
    public static final String samsung955loginMSSISDN = "9250133190";
    //msisdn который отображается в настройках (для сравнивания в тестах):
    public static final String samsung955FMSISDN = "+7 925 013 31 90";
    //полнове название модели:
    public static final String samsung955FModel = "SAMSUNG Galaxy Note S8+ (SM-N955F)";

    //msisdn samsung SM-N950F:, который используется для вставки при регистрации:
    public static final String samsung950loginMSSISDN = "9298324304";
    //msisdn который отображается в настройках (для сравнивания в тестах):
    public static final String samsung950FMSISDN = "+7 929 832 43 04";
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    //------------------------------------------>Цифровая клавиатура (СТАТИЧЕСКИЕ МЕТОДЫ):<-------------------------------

    //Нажатие кнопки '0':
    public static void pushButton0OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(29)
                .resourceId("ru.megalabs.multifon:id/n0_t")
                .text("0").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '0' успешно нажата " + (char)27 + "[0m");
    }
    //Длительное нажатие кнопки '0', для выбора символа '+':
    public static void longPressButton0OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(29)
                .resourceId("ru.megalabs.multifon:id/n0_t")
                .text("0").build());
        pushNumberButton.longPress();
        System.out.println((char) 27 + "[32mКнопка '0' успешно удержана " + (char)27 + "[0m");
    }
    //Нажатие кнопки '1':
    public static void pushButton1OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(1)
                .resourceId("ru.megalabs.multifon:id/n1_t")
                .text("1").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '1' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки '2':
    public static void pushButton2OnKeyBoard (String deviceID, String buttonNumberForLog) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(25)
                .resourceId("ru.megalabs.multifon:id/n2_t")
                .text("2").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '2' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки '3':
    public static void pushButton3OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(5)
                .resourceId("ru.megalabs.multifon:id/n3_t")
                .text("3").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '3' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки '4':
    public static void pushButton4OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(31)
                .resourceId("ru.megalabs.multifon:id/n4_t")
                .text("4").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '4' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки '5':
    public static void pushButton5OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(11)
                .resourceId("ru.megalabs.multifon:id/n5_t")
                .text("5").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '5' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки '6':
    public static void pushButton6OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(14)
                .resourceId("ru.megalabs.multifon:id/n6_t")
                .text("6").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '6' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки '7':
    public static void pushButton7OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(40)
                .resourceId("ru.megalabs.multifon:id/n7_t")
                .text("7").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '7' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки '8':
    public static void pushButton8OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(20)
                .resourceId("ru.megalabs.multifon:id/n8_t")
                .text("8").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '8' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки '9':
    public static void pushButton9OnKeyBoard (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button pushNumberButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Label")
                .mobileCenterIndex(23)
                .resourceId("ru.megalabs.multifon:id/n9_t")
                .text("9").build());
        pushNumberButton.tap();
        System.out.println((char) 27 + "[32mКнопка '9' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки 'посыл вызова':
    public static void pushMakeCallButton (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        UiObject makeCallButton = eMotionApplication.describe(UiObject.class, new UiObjectDescription.Builder()
                        .className("ImageView")
                        .mobileCenterIndex(3)
                        .resourceId("ru.megalabs.multifon:id/make_call").build());
        makeCallButton.tap();
        System.out.println((char) 27 + "[32mКнопка 'Посыл вызова' успешно нажата " + (char)27 + "[0m");
    }
    //Нажатие кнопки 'посыл вызова':
    public static void pushDropCallButton (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        UiObject dropCallButton = eMotionApplication.describe(UiObject.class, new UiObjectDescription.Builder()
                .className("ImageView")
                .mobileCenterIndex(5)
                .resourceId("ru.megalabs.multifon:id/button_drop_call").build());
        dropCallButton.tap();
        System.out.println((char) 27 + "[32mКнопка 'Завершение вызова' успешно нажата " + (char) 27 + "[0m");
    }
    //Нажатие кнопки 'Позвонить', при варнинге о плохом качестве сети:
    public static void pushForceCallButton (String deviceID) throws Exception
    {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button forceCallButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Button")
                .mobileCenterIndex(1)
                .resourceId("android:id/button1")
                .text("ПОЗВОНИТЬ").build());
        forceCallButton.tap();
        System.out.println((char) 27 + "[32mКнопка 'Позвонить при плохом качестве сети' успешно нажата " + (char)27 + "[0m");
    }
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*О С Н О В Н Ы Е С Т А Т И Ч Е С К И Е  М Е Т О Д Ы*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //----------------------------------------------------------------------------------------------------
    //Нажатие кнопки 'ДАЛЕЕ' при запуске приложения:
    public static void pushMainActivityNextButton (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button mainActivityNextButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Button")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/nextSlide")
                .text("ДАЛЕЕ").build());
        mainActivityNextButton.tap();
    }
    //Нажатие кнопки 'НАЧАТЬ' при запуске приложения:
    public static void pushMainActivityStartButton (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button mainActivityStartButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Button")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/nextSlide")
                .text("НАЧАТЬ").build());
        mainActivityStartButton.tap();
    }

    //Подтверждение любого пуш-разраешения:
    public static void pushConfirmAnyAcessButton (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button confirmAnyAccessButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Button")
                .mobileCenterIndex(1)
                .resourceId("com.android.packageinstaller:id/permission_allow_button")
                .text("РАЗРЕШИТЬ").build());
        confirmAnyAccessButton.tap();
    }
    //Метод для ввода MSISDN:
    public static void setMSISDNinLoginField (String deviceID, String loginDeviceMsisdn) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        EditField loginField = eMotionApplication.describe(EditField.class, new EditFieldDescription.Builder()
                        .className("Input")
                        .mobileCenterIndex(0)
                        .resourceId("ru.megalabs.multifon:id/editLogin").build());
        loginField.setFocus();
        loginField.setText(loginDeviceMsisdn);
    }

    //нажатие кнопки 'ВОЙТИ' после ввода MSISDN:
    public static void pushLogIn (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button loginButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Button")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/button_enter")
                .text("ВОЙТИ").build());
        loginButton.tap();
    }
    //метод для переключения на вкладку 'контакты':
    public static void pushContactsTab (String deviceID)throws Exception{
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label contactsTab = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(1)
                .resourceId("ru.megalabs.multifon:id/tab_name")
                .text("КОНТАКТЫ").build());
        contactsTab.tap();
    }
    //метод для переключения на вкладку 'вызовы':
    public static void pushCallsTab (String deviceID)throws Exception{
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label callsTab = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(2)
                .resourceId("ru.megalabs.multifon:id/tab_name")
                .text("ВЫЗОВЫ").build());
        callsTab.tap();
    }
    //метод для нажатия на кнопку 'цифровая клавиатура':
    public static void pushShowKeyboard (String deviceID)throws Exception{
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button showKeyboard = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("ImageButton")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/keypad").build());
        System.out.println((char) 27 + "[32mкнопка 'Цифровая клавиатура' выбрана успешно " + (char)27 + "[0m");
        showKeyboard.tap();
    }
    //------------------------------------------ЧАТЫ (СМС)-----------------------------------------------------------------
    //метод для переключения на вкладку 'чаты':
    public static void pushChatsTab (String deviceID)throws Exception{
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label chatsTab = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/tab_name")
                .text("ЧАТЫ").build());
        chatsTab.tap();
    }
    //метод для вставки смс сообщения в чат адресата:
    public static void typeSmsForm (String deviceID, String deviceModel, String appVersion, String buildVersion)throws Exception{
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        EditField messageInputEditField = eMotionApplication.describe(EditField.class, new EditFieldDescription.Builder()
                .className("Input")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/message_input").build());
        messageInputEditField.setFocus();
        messageInputEditField.setText("Тест на отправку смс из Emotion успешен, модель аппарата: " + deviceModel + " версия приложения: " + appVersion + " версия сборки: " + buildVersion);
    }
    //метод нажатия кнопки 'новый чат':
    public static void pushNewChatButton (String deviceID) throws Exception{
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button newChatButton = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("ImageButton")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/new_chat").build());
        newChatButton.tap();
        System.out.println((char) 27 + "[32mкнопка 'новый чат' выбрана успешно " + (char)27 + "[0m");
    }
    //метод для выбора чата техподдержки:
    public static void selectTSChat (String deviceID) throws Exception{
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label techSuppChat = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(2)
                .resourceId("ru.megalabs.multifon:id/userInit")
                .text("МТ").build());
        techSuppChat.tap();
        System.out.println((char) 27 + "[32mчат техподдержки выбран успешно " + (char)27 + "[0m");
    }
    //нажатие кнопки отправить смс:
    public static void pushSendSmS (String deviceID)throws Exception{
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        UiObject sendUiObject = eMotionApplication.describe(UiObject.class, new UiObjectDescription.Builder()
                .className("ImageView")
                .mobileCenterIndex(2)
                .resourceId("ru.megalabs.multifon:id/send").build());
        sendUiObject.tap();
        System.out.println((char) 27 + "[32mСмс успешно отправлено! " + (char)27 + "[0m");
    }
    //нажатие кнопки 'назад' после отправки смс:
    public static void pushBackFromSmsChat (String deviceID)throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        UiObject activityBackUiObject = eMotionApplication.describe(UiObject.class, new UiObjectDescription.Builder()
                .className("ImageView")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/activity_back").build());
        activityBackUiObject.tap();
        System.out.println((char) 27 + "[32mКнопка 'назад' усешно нажата " + (char) 27 + "[0m");
    }
    //удержание плитки чата:
    public static void holdChatBar (String deviceID, String chatName)throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label firstBarChat = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(20)
                .resourceId("ru.megalabs.multifon:id/userInit")
                .text(chatName).build());
        firstBarChat.longPress();
        System.out.println((char) 27 + "[32mПлитка чата " + chatName + (char) 27 + "[0m" + (char) 27 + "[32m успешно выбрана " + (char) 27 + "[0m");
    }
    //отметить радиобаттон нужного чата, используя нужный индекс:
    public static void markChatBar (String deviceID, String chatName, int barMobileCenterIndex)throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        UiObject selectorRadioButtonChat = eMotionApplication.describe(UiObject.class, new UiObjectDescription.Builder()
                .className("ImageView")
                .mobileCenterIndex(barMobileCenterIndex)
                .resourceId("ru.megalabs.multifon:id/selector").build());
        selectorRadioButtonChat.tap();
        System.out.println((char) 27 + "[32mПлитка чата " + chatName + (char) 27 + "[0m" + (char) 27 + "[32m успешно выбрана " + (char) 27 + "[0m");
    }
    //нажимаем 'удалить' чат:
    public static void deleteSelectedChat (String deviceID, String chatName)throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label deleteChatButton = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(59)
                .resourceId("ru.megalabs.multifon:id/cm_delete")
                .text("Удалить").build());
        deleteChatButton.tap();
        System.out.println((char) 27 + "[32mЧат " + chatName + (char) 27 + "[0m" + (char) 27 + "[32m успешно удалён. " + (char) 27 + "[0m");
    }
    //подтверждение удаления чата:
    public static void confirmDeleteSelectedChat (String deviceID, String chatName)throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        Button deletChatBarButtonConfirm = eMotionApplication.describe(Button.class, new ButtonDescription.Builder()
                .className("Button")
                .mobileCenterIndex(1)
                .resourceId("android:id/button1")
                .text("УДАЛИТЬ").build());
        deletChatBarButtonConfirm.tap();
        System.out.println((char) 27 + "[32mПодтверждение удаления осуществлено успешно " + (char)27 + "[0m");
    }
//-----------------------------------------------------------------------------------------------------------------

    //метод для клика на выпадающее меню:
    public static void pushDropDownMenu (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        DropDown dropdownButtonMenu = eMotionApplication.describe(DropDown.class, new DropDownDescription.Builder()
                .className("ImageView")
                .mobileCenterIndex(4)
                .resourceId("ru.megalabs.multifon:id/dropdown").build());
        dropdownButtonMenu.tap();
        System.out.println((char) 27 + "[32mВыпадающее меню успешно вызвано " + (char)27 + "[0m");
    }
    //метод для клика на значек eMotion:
    public static void pusheMotionLogo (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label eMotionLabel = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new com.hp.lft.sdk.mobile.LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/text_logo")
                .text("eMotion").build());
        eMotionLabel.tap();
    }
    //метод для перехода обратно с экрана с версиями eMotion:
    public static void pusheBackFromMotionLogo (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        UiObject backFromMotionLogo = eMotionApplication.describe(UiObject.class, new UiObjectDescription.Builder()
                .className("ImageView")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/back_button").build());
        backFromMotionLogo.tap();
        System.out.println((char) 27 + "[32mКнопка 'назад' успешно нажата " + (char) 27 + "[0m");
    }

    //метод получения версии приложения:
    public static String getAppVersion (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label eMotionLabel = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new com.hp.lft.sdk.mobile.LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/text_logo").build());
        System.out.println((char) 27 + "[32mВерсия приложения: " + (char)27 + "[0m" + eMotionLabel.getText());
        return eMotionLabel.getText();
    }
    //метод получения версии сборки:
    public static String getBuildVersion (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label buildVersion = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new com.hp.lft.sdk.mobile.LabelDescription.Builder()
                        .className("Label")
                        .mobileCenterIndex(1)
                        .resourceId("ru.megalabs.multifon:id/date").build());
        System.out.println((char) 27 + "[32mВерсия сборки: " + (char)27 + "[0m" + buildVersion.getText());
        return buildVersion.getText();
    }
    //метод который возвращает значение MSISDN из настроек приложения (выпадающее меню):
    public static String getTextMSISDN (String deviceID) throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(eMotionIdentifier, deviceID);
        com.hp.lft.sdk.mobile.Label msisdn = eMotionApplication.describe(com.hp.lft.sdk.mobile.Label.class, new LabelDescription.Builder()
                .className("Label")
                .mobileCenterIndex(0)
                .resourceId("ru.megalabs.multifon:id/number_text").build());
        return msisdn.getText();
    }

    //----------------------------------------------------------------------------------------------------

    //ожидание n-времени
    public static void sleepX (long x) throws Exception {
        Thread.sleep(x);
    }

    //----------------------------------------------------------------------------------------------------

    //метод инициализации девйса и приложения:
    public static Application initDeviceAndApp (String identifier, String deviceId) throws Exception {
        Device device = MobileLab.lockDeviceById(deviceId);
        Application eMotionApplication = device.describe(Application.class, new ApplicationDescription.Builder()
                .identifier(identifier)
                .packaged(false).build());
        return eMotionApplication;
    }
    //----------------------------------------------------------------------------------------------------
    //Метод для авторизации в приложении, на любом девайсе:
    public static void anyDeviceAuthorisation (String deviceID, String loginDeviceMsisdn)throws Exception{
        Device device = MobileLab.lockDeviceById(deviceID);
        Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, deviceID);
        //если есть кнопки далее нажимаем их:
        try {
            // ДАЛЕЕ
            pushMainActivityNextButton(deviceID);
            // ДАЛЕЕ
            pushMainActivityNextButton(deviceID);
            // НАЧАТЬ
            pushMainActivityStartButton(deviceID);
        } catch (GeneralLeanFtException e) {
            System.out.println((char) 27 + "[32mКнопок 'ДАЛЕЕ' нет, авторизация уже выполнялась первый раз. " + (char)27 + "[0m");
        }

        //разрешение на отправку и прием сообщений:
        try {
            pushConfirmAnyAcessButton(deviceID);
        } catch (GeneralLeanFtException exSMS) {
            System.out.println((char) 27 + "[32mПодтверджение 'разрешить eMotion отправку смс' было дано ранее. " + (char)27 + "[0m");
        }
        // вводим номер телефона:
        setMSISDNinLoginField(deviceID,loginDeviceMsisdn);
        // нажимаем кнопку 'войти':
        pushLogIn(deviceID);
        //ждем 30 секунд, пока произойдет авторизация:
                sleepX(30);
        //разрешаем доступ к контактам, если приложение запрашивает:
        try {
            pushConfirmAnyAcessButton(deviceID);
        } catch (GeneralLeanFtException exContacts) {
            System.out.println((char) 27 + "[32mРазрешение на доступ к контактам было дано ранее.(1/5) " + (char)27 + "[0m");
        }
        //разрешаем доступ к записи аудио, если приложение запрашивает:
        try {
            pushConfirmAnyAcessButton(deviceID);
        } catch (GeneralLeanFtException exContacts) {
            System.out.println((char) 27 + "[32mРазрешение на доступ к записи аудио было дано ранее.(2/5) " + (char)27 + "[0m");
        }
        //разрешаем доступ к звонкам, если ранее не было дано:
        try {
            pushConfirmAnyAcessButton(deviceID);
        } catch (GeneralLeanFtException exCalls) {
            System.out.println((char) 27 + "[32mРазрешение на доступ к звонкам было дано ранее.(3/5) " + (char)27 + "[0m");
        }
        //разрешаем доступ к фото, если ранее не было дано:
        try {
            pushConfirmAnyAcessButton(deviceID);
        } catch (GeneralLeanFtException exPhotos) {
            System.out.println((char) 27 + "[32mРазрешение на доступ к фото было дано ранее.(4/5) " + (char)27 + "[0m");
        }
        //разрешаем доступ к местоположению, если ранее не было дано:
        try {
            pushConfirmAnyAcessButton(deviceID);
        } catch (GeneralLeanFtException exGeolocation) {
            System.out.println((char) 27 + "[32mРазрешение на доступ к местоположению было дано ранее.(4/5) " + (char)27 + "[0m");
        }

    }
    //----------------------------------------------------------------------------------------------------

    //Метод для ПЕРЕУСТАНОВКИ приложения:
    public static void reinstalleMotionApp (String deviceID) throws Exception {

        Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, deviceID);
        eMotionApplication.uninstall();
        eMotionApplication.install();

    }

    //----------------------------------------------------------------------------------------------------
}