import com.hp.lft.sdk.mobile.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.hp.lft.sdk.*;
import com.hp.lft.verifications.*;

import unittesting.*;

                            /*                   Тест для проверки тестов
                           -------------------------------------------------------------------------------
                            Тест заполняется куском кода теста, который необходимо проверить.
                                                        Описание среды:
                            происходит на девайсе SAMSUNG 955F с номером: 9250133190
                                                       Аннотация к тесту:
                           Смена девайса в тесте происходит по средставм смены значения переменной testID, объявленной
                           сразу после создания класса: private String deviceTestID = MainClass.<IDдевайса>
                                                       Переменные:
                           Так же значение логина (MSISDN) зависит от конкретного аппарата и помещается в значение:
                           private String deviceLoginMsisdn = MainClass.<девайсLoginMSISDN>
                           -------------------------------------------------------------------------------
                           */

public class TestForTests extends UnitTestClassBase{

    //объявляем для данного класса тестов девайс:
    private String deviceTestID = MainClass.iDSamsungSMN955F;
    private String deviceLoginMsisdn = MainClass.samsung955loginMSSISDN;
    private String deviceModel = MainClass.samsung955FModel;


        @BeforeClass
        public static void setUpBeforeClass() throws Exception {
            instance = new LeanFtTest();
            globalSetup(LeanFtTest.class);

        }

        @AfterClass
        public static void tearDownAfterClass() throws Exception {
            globalTearDown();
        }

        @Before
        public void setUp() throws Exception {
            //инициализируем и лочим девайс:
            Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, MainClass.iDSamsungSMN955F);
            //запускаем приложение:
            eMotionApplication.launch();
        }

        @After
        public void tearDown() throws Exception {
            Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, MainClass.iDSamsungSMN955F);
        }

        @Test
        public void smsSendTestOfTest() throws Exception {
                //инициализируем и лочим девайс, и указываем нужное приложение:
                Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, deviceTestID);
                //узнаем версию приложения и сборки и помещаем их в переменные:
                eMotionApplication.restart();
                MainClass.pusheMotionLogo(deviceTestID);
                String appVer = MainClass.getAppVersion(deviceTestID);
                String buildVer = MainClass.getBuildVersion(deviceTestID);
                //Возвращаемся с экрана версий:
                MainClass.pusheBackFromMotionLogo(deviceTestID);
                //переходим в 'чаты'
                MainClass.pushChatsTab(deviceTestID);
                // нажимаем кнопку 'новый чат':
                MainClass.pushNewChatButton(deviceTestID);
                // выбираем чат техподдержки:
                MainClass.selectTSChat(deviceTestID);
                //передаем в поле для сообщения свой текст:
                MainClass.typeSmsForm(deviceTestID, deviceModel, appVer, buildVer);
                //нажимаем отправить:
                MainClass.pushSendSmS(deviceTestID);
                //нажимаем 'назад':
                MainClass.pushBackFromSmsChat(deviceTestID);
                // удерживаем плитку нужного чата техподдержки для вызова радиобаттонов:
                MainClass.holdChatBar(deviceTestID, "МТ");
                //-*-*-*-*-*-*-*-*-*-*-*-*-удаление-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
                // отмечаем радиобаттон чата техподдержки
                MainClass.markChatBar(deviceTestID, "МТ",3);
                // нажимаем 'удалить':
                MainClass.deleteSelectedChat(deviceTestID,"МТ");
                // отмечаем радиобаттон чата техподдержки
                 MainClass.markChatBar(deviceTestID, "МТ",3);
                // нажимаем 'удалить':
                MainClass.deleteSelectedChat(deviceTestID,"МТ");
                // подтверждение удаления
                MainClass.confirmDeleteSelectedChat(deviceTestID,"МТ");

    }

}
