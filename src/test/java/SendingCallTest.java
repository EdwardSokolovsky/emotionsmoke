import com.hp.lft.sdk.mobile.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.hp.lft.sdk.*;
import com.hp.lft.verifications.*;

import unittesting.*;

                           /*                       Готовность теста: 100%
                           -------------------------------------------------------------------------------
                                                        Описание среды:
                           Тест на посыл вызова, происходит на девайсе SAMSUNG 955F с номером: 9250133190
                                                       Аннотация к тесту:
                           Смена девайса в тесте происходит по средставм смены значения переменной testID, объявленной
                           сразу после создания класса: private String deviceTestID = MainClass.<IDдевайса>
                                                        Переменные:
                           Так же значение логина (MSISDN) зависит от конкретного аппарата и помещается в значение:
                           private String deviceLoginMsisdn = MainClass.<девайсLoginMSISDN>
                           -------------------------------------------------------------------------------
                           */

public class SendingCallTest extends UnitTestClassBase{

    //объявляем для данного класса тестов девайс:
    private String deviceTestID = MainClass.iDSamsungSMN955F;
    private String deviceLoginMsisdn = MainClass.samsung955loginMSSISDN;


    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        instance = new LeanFtTest();
        globalSetup(LeanFtTest.class);

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        globalTearDown();
    }

    @Before
    public void setUp() throws Exception {
        //инициализируем и лочим девайс:
        Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, deviceTestID);
        //запускаем приложение:
        eMotionApplication.launch();
        //Авторизируемся
        try {
            MainClass.anyDeviceAuthorisation(deviceTestID,deviceLoginMsisdn);
        } catch (ReplayObjectNotFoundException exAuthorisationFailed) {
            System.out.println("Авторизация уже была выполнена");
            eMotionApplication.restart();
        }

    }

    @After
    public void tearDown() throws Exception {
        Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, deviceTestID);
        //возвращаем к исходному состоянию:
        MainClass.reinstalleMotionApp(deviceTestID);
        eMotionApplication.kill();
    }

    @Test
    public void SimpleRingtest() throws Exception {

        //инициализируем девайс и приложение:
        Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, deviceTestID);

        //выбираем вкладку 'вызовы':
        MainClass.pushCallsTab(deviceTestID);

        //нажимаем на кнопку "клавиатура:"
        MainClass.pushShowKeyboard(deviceTestID);

        //--------------------------->набираем номер:<-------------------------------
        //нажимаем кнопку '+':
        MainClass.longPressButton0OnKeyBoard(deviceTestID);
        //нажимаем кнопку '7':
        MainClass.pushButton7OnKeyBoard(deviceTestID);
        //нажимаем кнопку '9':
        MainClass.pushButton9OnKeyBoard(deviceTestID);
        //нажимаем кнопку '1':
        MainClass.pushButton1OnKeyBoard(deviceTestID);
        //нажимаем кнопку '6':
        MainClass.pushButton6OnKeyBoard(deviceTestID);
        //нажимаем кнопку '1':
        MainClass.pushButton1OnKeyBoard(deviceTestID);
        //нажимаем кнопку '3':
        MainClass.pushButton3OnKeyBoard(deviceTestID);
        //нажимаем кнопку '1':
        MainClass.pushButton1OnKeyBoard(deviceTestID);
        //нажимаем кнопку '8':
        MainClass.pushButton8OnKeyBoard(deviceTestID);
        //нажимаем кнопку '5':
        MainClass.pushButton5OnKeyBoard(deviceTestID);
        //нажимаем кнопку '8':
        MainClass.pushButton8OnKeyBoard(deviceTestID);
        //нажимаем кнопку '5':
        MainClass.pushButton5OnKeyBoard(deviceTestID);
        //нажимаем "посыл вызова":
        MainClass.pushMakeCallButton(deviceTestID);
        try {
            MainClass.pushForceCallButton(deviceTestID);
        } catch (GeneralLeanFtException warningExeption) {
            System.out.println((char) 27 + "[32mКачество сети приемлемое, поэтому Warning не появился " + (char)27 + "[0m");
        }
        //ожидаем дозвона (дадим 8 секунд)
        MainClass.sleepX(8000);


        //завершение вызова:
        MainClass.pushDropCallButton(deviceTestID);
        //отрапортуем о результате:
        System.out.println((char) 27 + "[32mТест на посыл вызова выполнен успешно! " + (char)27 + "[0m");

    }


}