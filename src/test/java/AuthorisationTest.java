import com.hp.lft.sdk.mobile.*;
import com.hp.lft.sdk.mobile.Button;
import com.hp.lft.sdk.mobile.Label;
import com.hp.lft.sdk.sap.gui.ToolBar;
import org.junit.*;
import com.hp.lft.sdk.*;
import com.hp.lft.verifications.*;

import unittesting.*;

import java.awt.*;


                           /*                        Готовность теста: 100%
                           -------------------------------------------------------------------------------
                                                       Описание среды:
                           Тест на авторизацию, происходит на девайсе SAMSUNG 955F с номером: 9250133190
                                                       Аннотация к тесту:
                           Смена девайса в тесте происходит по средставм смены значения переменной testID, объявленной
                           сразу после создания класса: private String deviceTestID = MainClass.<IDдевайса>

                           Так же значение логина (MSISDN) зависит от конкретного аппарата и помещается в значение:
                           private String deviceLoginMsisdn = MainClass.<девайсLoginMSISDN>
                                                        Переменные:
                           Дополнительно необходима переменная assertDeviceLoginMsisdn, в которую помещается значение
                           MSISDN из MainClass: private String assertDeviceLoginMsisdn
                           -------------------------------------------------------------------------------
                            */


public class AuthorisationTest extends UnitTestClassBase {

    //объявляем для данного класса тестов девайс:
    private String deviceTestID = MainClass.iDSamsungSMN955F;
    //MSISDN для вбивание в поле логина:
    private String deviceLoginMsisdn = MainClass.samsung955loginMSSISDN;
    //в данном случае так же необходим MSISDN для сравнения:
    private String assertDeviceLoginMsisdn = MainClass.samsung955FMSISDN;


    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        instance = new LeanFtTest();
        globalSetup(LeanFtTest.class);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        globalTearDown();
    }

    @Before
    public void setUp() throws Exception {
        //инициализируем и лочим девайс:
        Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, deviceTestID);
        //запускаем приложение:
        eMotionApplication.launch();
    }

    @After
    public void tearDown() throws Exception {
        MainClass.reinstalleMotionApp(deviceTestID);
    }

    @Test
    public void eMotionAndroidAuthorisationTest() throws Exception {
        //инициализируем приложение:
        Application eMotionApplication = MainClass.initDeviceAndApp(MainClass.eMotionIdentifier, deviceTestID);

        //Авторизируемся, используем метод из MainClass:
        try {
            MainClass.anyDeviceAuthorisation(deviceTestID,deviceLoginMsisdn);
        } catch (ReplayObjectNotFoundException exAuthorisationFailed) {
            System.out.println((char) 27 + "[31mАвторизация уже была выполнена, необходима переустановка приложения! " + (char)27 + "[0m");
            MainClass.reinstalleMotionApp(deviceTestID);
            System.out.println((char) 27 + "[32mПриложение успешно переустановлено! " + (char)27 + "[0m");
            eMotionApplication.launch();
            System.out.println((char) 27 + "[32mПриложение успешно запущено. " + (char)27 + "[0m");
        }

        //кликаем на выпадающее меню:
        MainClass.pushDropDownMenu(deviceTestID);
        //получем текст залогиненного MSISDN и помещаем его в переменную gettingMsisdn:
        String gettingMsisdn = MainClass.getTextMSISDN(deviceTestID);
        //сверяем номер телефона, который прошел авторизацию, с номером MSISDN девайса из MainClass:
        Assert.assertEquals(assertDeviceLoginMsisdn, gettingMsisdn);

    }

}