##eMotion Smoke 1.1
���������������� suit ��� ���������� ���������� ���������� Android-���������� eMotion, �� �������� ��������, �������� � ����:

AuthorisationTest (���� �� ���������� �����������):
---
* ������ ����������;

* ���� ������;

* ���-�������������;

* �������� �����������;

* ����������� � ����������� ��������.

SmsSendTest (���� �� �������� ���) + ����������� ����������� �����:
---

* ������ ����������;

* �����������;

* ���� � '����';

* �������� ����;

* ��������� ���;

* ������� ���;

* ����������� � ����������� ��������.

[![My Autotest video-example](http://img.youtube.com/vi/DJE1MCEkgQU/0.jpg)](https://www.youtube.com/watch?v=DJE1MCEkgQU)

SendingCallTest (���� �� ����� ������) + ����������� ����������� �����:
---
* ������ ����������;

* �����������;

* ���� � '������';

* ������� '����������';

* ����� ������;

* ����� ������;

* ������� �� ����� �����;

* ����� ������;

* ����������� � ����������� ��������.

[![My Autotest video-example](http://img.youtube.com/vi/Bt1NePBcEBA/0.jpg)](https://www.youtube.com/watch?v=Bt1NePBcEBA)